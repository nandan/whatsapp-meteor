import { Component } from "@angular/core";
import { ChatsComponent } from "../chats/chats.component";
import template from "./tabscontainer.component.html";

@Component({
  selector: "tabs-container",
  template
})
export class TabsContainerComponent {
  chatsRoot = ChatsComponent;

  constructor() {

  }
}
