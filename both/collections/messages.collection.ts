import { Message } from "../models/message.model";
import { MongoObservable } from "meteor-rxjs";

export const Messages = new MongoObservable.Collection<Message>('messages');
